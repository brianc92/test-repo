package demo.bookservice;

import com.fasterxml.jackson.core.JsonProcessingException;
import demo.bookservice.model.Book;
import io.micronaut.http.HttpRequest;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.client.RxHttpClient;
import io.micronaut.http.client.annotation.Client;
import io.micronaut.test.extensions.junit5.annotation.MicronautTest;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;

import static org.junit.jupiter.api.Assertions.assertEquals;

@MicronautTest
public class BookControllerTest {

  @Inject TestConfiguration testConfig;

  @Inject
  @Client("/")
  RxHttpClient client;

  @Test
  void name() {}

  @AfterAll
  public static void cleanupSpec() {}

  @Test
  void testSearchBook() throws JsonProcessingException {
    HttpResponse<Book> response =
        this.client.toBlocking().exchange(HttpRequest.GET("/?name=the"), Book.class);

    assertEquals(200, response.code(), "Should be 200 OK");
    assertEquals(
        this.testConfig.getTestBook().getIsbn(),
        response.body().getIsbn(),
        "Response ISBN should be the same as the test book's ISBN");
  }
}
