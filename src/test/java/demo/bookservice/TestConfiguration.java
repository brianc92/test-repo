package demo.bookservice;

import com.amazonaws.services.dynamodbv2.local.embedded.DynamoDBEmbedded;
import demo.bookservice.config.DynamoClientProvider;
import demo.bookservice.model.Book;
import io.micronaut.context.annotation.Factory;
import io.micronaut.context.annotation.Replaces;
import lombok.Getter;
import software.amazon.awssdk.services.dynamodb.DynamoDbClient;

import javax.inject.Singleton;

@Factory
@Getter
public class TestConfiguration {

  private final Book testBook =
      new Book(
          "isbn-1234",
          "The Book Name",
          100,
          "2014-06-17T00:00:00",
          "https://anapioficeandfire.com/api/books/other");

  @Singleton
  @Replaces(DynamoDbClient.class)
  public DynamoDbClient dynamoDbClient() {
    return DynamoDBEmbedded.create().dynamoDbClient();
  }

  @Singleton
  @Replaces(DynamoClientProvider.class)
  public DynamoClientProvider dynamoClientProvider() {
    DynamoClientProvider provider = new DynamoClientProvider(dynamoDbClient());
    provider.getBookTable().createTable();
    provider.getBookTable().putItem(getTestBook());
    return provider;
  }
}
