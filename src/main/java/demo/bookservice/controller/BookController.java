package demo.bookservice.controller;

import demo.bookservice.model.Book;
import demo.bookservice.service.BookService;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.QueryValue;
import lombok.AllArgsConstructor;

import java.util.List;

@Controller(value = "/")
@AllArgsConstructor
public class BookController {

  BookService bookService;

  @Get
  public List<Book> searchBooksByName(@QueryValue final String name) {
    return bookService.searchBooksByName(name);
  }
}
