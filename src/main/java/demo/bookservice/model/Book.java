package demo.bookservice.model;

import io.micronaut.core.annotation.Introspected;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import software.amazon.awssdk.enhanced.dynamodb.mapper.annotations.DynamoDbBean;
import software.amazon.awssdk.enhanced.dynamodb.mapper.annotations.DynamoDbPartitionKey;

@Introspected
@DynamoDbBean
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Book {

  private String isbn;
  private String name;
  private int numberOfPages;
  private String released;
  private String url;

  @DynamoDbPartitionKey
  public String getIsbn() {
    return this.isbn;
  }
}
