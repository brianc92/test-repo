package demo.bookservice.service;

import demo.bookservice.config.DynamoClientProvider;
import demo.bookservice.model.Book;
import lombok.AllArgsConstructor;

import javax.inject.Singleton;
import java.util.List;
import java.util.stream.Collectors;

@Singleton
@AllArgsConstructor
public class BookService {

  DynamoClientProvider dynamoClientProvider;

  public List<Book> searchBooksByName(final String name) {
    return dynamoClientProvider.getBookTable().scan().items().stream()
        .filter(book -> book.getName().toLowerCase().contains(name.toLowerCase()))
        .collect(Collectors.toList());
  }
}
