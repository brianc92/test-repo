package demo.bookservice.config;

import demo.bookservice.model.Book;
import lombok.Getter;
import software.amazon.awssdk.enhanced.dynamodb.DynamoDbEnhancedClient;
import software.amazon.awssdk.enhanced.dynamodb.DynamoDbTable;
import software.amazon.awssdk.enhanced.dynamodb.TableSchema;
import software.amazon.awssdk.services.dynamodb.DynamoDbClient;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
@Getter
public class DynamoClientProvider {

  // look into: io.micronaut.aws.sdk.v2.service.dynamodb
  // DynamoDB client factory.
  private final DynamoDbClient dynamoClient;

  private final DynamoDbEnhancedClient enhancedClient;

  private final TableSchema<Book> bookTableSchema;

  private final DynamoDbTable<Book> bookTable;

  @Inject
  public DynamoClientProvider(DynamoDbClient dynamoClient) {
    this.dynamoClient = dynamoClient;
    this.enhancedClient = DynamoDbEnhancedClient.builder().dynamoDbClient(this.dynamoClient).build();
    this.bookTableSchema = TableSchema.fromBean(Book.class);
    this.bookTable = this.enhancedClient.table("Book", this.bookTableSchema);
  }
}
